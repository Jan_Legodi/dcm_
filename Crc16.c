void crc(void);
void crc_first(void);


unsigned int CRC16;//,crc1,crc2;
unsigned char data1;


void crc16(unsigned char first,unsigned char last)
{
/* .......................... start of 16 bit CRC here ................................ */

   CRC16=0x0000;			/* preload ffff hex */
   data1=SBUF_BUFFER[first];
   crc_first();                         /* do calc on first byte of data */

   for(loop=first+1;loop <= last;loop++)
   {
	  data1=SBUF_BUFFER[loop] /**iptr*/;               /* do calc on rest of data */
      crc();
   }

/*   crc_hi=crc1;				/* set hi and low bytes for check */
/*   crc_lo=crc2; */


}




/* -------------------------- CRC generation --------------------------- */

void crc()
{
/* 'data1' is 1 of the 8 bit data bytes that is transmitted (RS232 / RS485) */
/* .... it is processed in another routine */

/* variables declared as 'data' for direct addressing .... */
unsigned int i,shifted_bit;


   CRC16=(CRC16 ^ data1);              /* 16bit CRC register */
   watchdog();                           /* reset watchdog timer */

   for (i = 8; i > 0; i--)            /* process 8 bits */
   {
      shifted_bit=(CRC16 & 0x001);    /* LSB before shift right */
      CRC16=(CRC16 >> 1);             /* shift CRC16 right one position */
      if(shifted_bit == 0x0001)       /* was shifted bit(LSB) of CRC16 == 1 */
      {
	 CRC16=(CRC16 ^ 0xa001);      /* Exclusive OR with A001 hex */
      }
      watchdog();                       /* reset watchdog timer */
   }

//   crc1=(CRC16 >> 8);                 /* upper 8 bit byte of CRC16 */
//   crc2=(CRC16 & 0x00ff);             /* lower 8 bit byte of CRC16 */
}


void crc_first()
{
/* 'data1' is first of the 8 bit data bytes .... */
/* .... that is transmitted (RS232 / RS485) ... */
/* .... it is processed in another routine      */

unsigned int crc1,crc2;

   crc1=(CRC16 >> 8);                 /* upper 8 bit byte of CRC16 */
   crc2=(CRC16 & 0x00ff);             /* lower 8 bit byte of CRC16 */
   crc2=(crc1 ^ data1);                /* CRC16 hibyte Exclusive OR with data */
   CRC16=((crc1 << 8) | crc2);        /* 16bit CRC register */
   watchdog();                        /* reset watchdog timer */
   data1=0;
   crc();
}


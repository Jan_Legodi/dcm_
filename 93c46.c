/************************************************************************/
/*                           93C46.C                                    */
/*                                                                      */
/*      Header file used for eeprom type 93C46 connected to 89C51/2051  */
/*                                                                      */
/*      ... declare I/O pins in main program ...CS,SK,DI,DO             */
/*                                                                      */
/*                                                                      */
/* ... this file modified for DCM2_01.C .....                           */
/************************************************************************/

void ewen(void);
void ewds(void);
void write_rom(unsigned char addr, unsigned char rdata);
unsigned char read_rom(unsigned char addr);
void start_bit(void);
void rom_clock(void);



/************************************************************************/
/*                      EWEN Function                                   */
/************************************************************************/

void ewen(void)
{
	unsigned char cnt;
	
	//init pins
	CS = 0;
	SK = 0;
	DI = 0;
	
	//first clock cycle
	rom_clock();
	
	//select chip
	start_bit();
	
	//clock in opcode
	DI = 0;                   /* op code 1 */
	rom_clock();
	DI = 0;                   /* op code 2 */
	rom_clock();
	
	//clock in command
	DI = 1;                   /* A5 */
	rom_clock();
	DI = 1;                   /* A6 */
	rom_clock();
	
	//clock through 4 unused bits
	cnt = 4;
	while(cnt > 0)
	{
		DI = 0;
		rom_clock();
		cnt--;
	}
	
	
/****************************************************\
  clocking a few more times seems to fix an unknown
     system fail where DCM stops communication
                   with SCM
	!!!TAKE CARE WHEN REMOVING OR CHANGING!!!
\****************************************************/

	cnt = 4;
	while(cnt > 0)
	{
		DI = 0;
		rom_clock();
		cnt--;
	}
	
	//deselect chip
	CS = 0;
	CS = 0;
	CS = 0;
}

/************************************************************************/
/*                      EWDS Function                                   */
/************************************************************************/


void ewds(void)
{
	unsigned char cnt;
	
	//init pins
	CS = 0;
	SK = 0;
	DI = 0;
	
	//first clock cycle
	rom_clock();
	
	//select chip
	start_bit();
	
	//clock in opcode
	DI = 0;                   /* op code 1 */
	rom_clock();
	DI = 0;                   /* op code 2 */
	rom_clock();
	
	//clock in command
	DI = 0;                   /* A5 */
	rom_clock();
	DI = 0;                   /* A6 */
	rom_clock();
	
	//clock through 4 unused bits
	cnt = 4;
	while(cnt > 0)
	{
		DI = 0;
		rom_clock();
		cnt--;
	}
	
	
/****************************************************\
  clocking a few more times seems to fix an unknown
     system fail where DCM stops communication
                   with SCM
	!!!TAKE CARE WHEN REMOVING OR CHANGING!!!
\****************************************************/

	cnt = 4;
	while(cnt > 0)
	{
		DI = 0;
		rom_clock();
		cnt--;
	}
	
	//deselect chip
	CS = 0;
	CS = 0;
	CS = 0;
}

/************************************************************************/
/*                      WRITE Function                                  */
/************************************************************************/

void write_rom(unsigned char addr, unsigned char rdata)
{
	unsigned char cnt;

	//enable write
	ewen();
	
	//init pins
	CS = 0;
	SK = 0;
	DI = 0;
	
	//first clock cycle
	rom_clock();
	
	//select chip
	start_bit();
	
	//clock in opcode
	DI = 0;                   /* op code 1 */
	rom_clock();
	DI = 1;                   /* op code 2 */
	rom_clock();

	//send 5 bit address
	cnt = 5;
	
	//shift address twice for alignment
	addr = addr << 2;
	
	while(cnt > 0)
	{
		DI = ((addr & 0x40)>>6);
		rom_clock();
		addr = addr << 1;
		cnt--;
	}

	//send 8 bit data
	cnt=8;
	while(cnt > 0)
	{
		DI = ((rdata & 0x80)>>7);
		rom_clock();
		rdata = rdata <<1;
		cnt--;
	}
	
	//pulse through unused 8 bits
	cnt = 8;
	DI = 0;
	while(cnt > 0)
	{
		rom_clock();
		cnt--;
	}
	
/****************************************************\
  clocking a few more times seems to fix an unknown
     system fail where DCM stops communication
                   with SCM
	!!!TAKE CARE WHEN REMOVING OR CHANGING!!!
\****************************************************/

	cnt = 4;
	while(cnt > 0)
	{
		DI = 0;
		rom_clock();
		cnt--;
	}
	
	//pulse select for busy indicator
	CS = 0;
	CS = 0;
	CS = 1;
	
	//wait while busy
	while(DO == 0){;} 
	CS = 0;
	
	//clear busy state
	CS = 0;
	CS = 1;
	//init pins
	CS = 0;
	SK = 0;
	DI = 0;

	//first clock cycle
	rom_clock();

	//clear ready state
	start_bit();
	
	//init pins
	CS = 0;
	SK = 0;
	DI = 0;
	
	//disable write
	ewds();

}


/************************************************************************/
/*                      READ Function                                   */
/************************************************************************/

unsigned char read_rom(unsigned char addr)
{
	unsigned char cnt;
	unsigned char val;
	
	//init pins
	CS = 0;
	SK = 0;
	DI = 0;

	//first clock cycle
	rom_clock();

	//select chip
	start_bit();
	
	//clock in opcode
	DI = 1;                   /* op code 1 */
	rom_clock();
	DI = 0;                   /* op code 2 */
	rom_clock();

	//send 5 bit address
	cnt = 5;
	
	//shift address twice for alignment
	addr = addr << 2;
	
	while(cnt  > 0)
	{
		DI = ((addr & 0x40)>>6);
		rom_clock();
		addr = addr << 1;
		cnt --;
	}
	
	//set up receive state
	DI = 0;
	cnt = 8;
	val = 0;

	//receive first 8 bits
	while(cnt  > 0)
	{
		rom_clock();
		val = val <<1;
		val = (val|DO);
		cnt --;
	}

	//clock through unused 8 bits
	cnt = 8;
	while(cnt > 0)
	{
		rom_clock();
		cnt--;
	}
	
/****************************************************\
  clocking a few more times seems to fix an unknown
     system fail where DCM stops communication
                   with SCM
	!!!TAKE CARE WHEN REMOVING OR CHANGING!!!
\****************************************************/

	cnt = 4;
	while(cnt > 0)
	{
		DI = 0;
		rom_clock();
		cnt--;
	}
	
	//deselect and return
	CS = 0;
	DI = 0;
	return(val);
}


void start_bit(void)
{
	//pull chip select and data in high to indicate start
	CS = 1;
	DI = 1;
	
	//clock once
	rom_clock();
}

void rom_clock(void)
{
	//pulse clock line
	SK=0;
	SK=1;
	SK=1;
	SK=0;
}







// ................... GLOBAL DEFINITIONS ...............................
// ......................................................................

// ..................... SERIAL BUFFER ..................................

#define BUFFER_SIZE			64
#define CR					0x0D
#define LF					0x0A
#define VIU_MARK			0x15
#define TAG_MARK			0x20
#define TAG_DATA_LENGTH		37
#define VIU_DATA_LENGTH		56
#define VIU_DATA_FG_NUMBER	30

// ......................................................................

// ........................ COMMS .......................................

#define	baud96  TH1=0xfd     // TH1 value for 9600bd 
#define baud24  TH1=0xf4     // TH1 value for 2400bd 
	       // baud rate : 300 = 0xa0 
	       //             600 = 0xd0 
	       //            1200 = 0xe8 
	       //            2400 = 0xf4 
	       //            4800 = 0xfa 
	       //            9600 = 0xfd 

#define  COMMS_TIMEOUT  (125)                //  X 0.06 s

// ......................................................................
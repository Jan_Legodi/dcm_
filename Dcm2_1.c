

// ...................................................................... 
//
//                           DCM2_125.C
//      
//
//          ****     NEW SMALL COMBINATION DCM - DCM2    ****
//                ... SMALL BOX 
// ... NB. change software version in "void software(void) ....
//                    #################################
//                          replaces DCM2AMT.C 
//                    #################################
//       D.C.M. 2            ....... PUMP CONTROL ONLY .... software ID ....    
//   ....+ Serial No  + TAG controller ....
//        
//                            Base Version 2.10                        
//                            Current Version 5.20                        
//
//                        Started :  02/03/1998                  
//
//                       Modified :  12/08/2019
//
//
//                             DCM5-20.PCB
//
//        !! WITH POWER FAIL (DCM2BAK.PCB) ......... 
//
//   ....  with Nozzle return delay  .... nozzle must be down for +-3 sec
//
//   ... fully variable pulser ratio , ID 0 - 32 (was only 1 - 9)
//
//
// ..... tag No can be between 0x30 ... 0x7a !!!! (for Thabazimbi)   changed 27/05/99
//
//  .... original VCU code had a "BUG" in 1st 2 chars of LRC check   changed 24/06/99
//
//  .... also reads the new VKC format with CRC- send to SCM with LRC
//     HCB 23032000 : Corrected the Halt command function to end transaction 
//     HCB 05042000 : Extended the data availability period from 80 to 200 cycles
//     HCB 05042000 : Inserted a LITRE_BUSY flag to avoid re-entry of fuel_calc
//     JP  22092000 : Updated version number to 115.
//     JP copied from Dcm2-1 for SLpos  (V2_116) 
// ...................................................................... 





// ........................ INCLUDE FILES ............................... 

#include <reg52.h>

// ...................................................................... 



// ........................ DEFINES ...................................... 

sbit	IR_LED		=	0x80;        // P0_0 - Infra Red LED - TAG READER
sbit	RED_LED		=	0x82;        // P0_1 - RED_LED - TAG_READER
sbit	OSC_CONTROL	=	0x81;        // P0_3 - OSC. Control - TAG READER	
sbit	COMMS_TX 	=	0x86;        // P0_6 - 75176 pin 2 
sbit	MOTOR    	=	0x87;        // P0_7 - relay 


sbit	COMMST      =  0x90;        //P1_0
sbit	COMMSS      =  0xa2;        //P2_2

sbit	CS      	=     0x94;        // P1_4 - CS of 93C46  
sbit	SK      	=     0x95;        // P1_5 - SK of 93C46  
sbit	WDG			=	  0x96;        // P1_6 - watchdog
sbit	DI      	=     0x97;        // P1_7 - DI of 93C46  


sbit	NZ_2WIRE_TX		=	    0xa0;        // P2_0 - 2 Wire NOZZLE TX CONTROL	
sbit	LED             =       0xa1;        // P2_1 - Buzzer
sbit	OVER_RIDE       =       0xa7;        // P2_2 - over-ride 
sbit	SYSTEM_COMMS    =       0xa3;        // P2_3 - System comms enable - 4066
sbit	NOZZLE_COMMS    =       0xa4;        // P2_4 - Nozzle comms enable - 4066 
sbit	POWER_FAIL      =       0xa5;        // P2_5 - Power fail signal
sbit	TAG_COMMS       =       0xa6;        // P2_6 - TAG comms enable - 4066 


sbit	TAG_PRESENT     =       0xb4;        // P3_4 - TAG data present
sbit	VCU_PRESENT     =       0xb5;        // P3_5 - TAG data present
sbit	NOZZLE_SW  	    =	    0xb6;        // P3_6 - nozzle switch 
sbit    DO      	    =       0xb7;        // P3_7 - DO of 93C46  

sbit	ID_2    	=	0xA0;        // P2_0 
sbit	ID_3		=	0xC2;        // P4_2
sbit	ID_4		=	0xC4;        // P4_4
sbit	ID_8		=	0x90;        //  
sbit	ID_7		=	0x91;        //   
sbit	ID_6   		=	0x92;        // P1_2 - 	
sbit	ID_5   		=	0x93;        // 



//#define	  	nozzle_id	(0x0f - (P1 & 0x0f))  // lower 4 bits of P1   
#define	baud96  TH1=0xfd     // TH1 value for 9600bd 
#define baud24  TH1=0xf4     // TH1 value for 2400bd 
	       // baud rate : 300 = 0xa0 
	       //             600 = 0xd0 
	       //            1200 = 0xe8 
	       //            2400 = 0xf4 
	       //            4800 = 0xfa 
	       //            9600 = 0xfd 

#define  COMMS_TIMEOUT  (125)                //  X 0.06 s

// ...................................................................... 



// ..................... FUNCTION DECLARATIONS .......................... 

void timer(unsigned int time);                 // delay routine 
void rx_interrupt(void);                       // comms interupt routine 
void LRC_calc(void);                           // LRC calculation routine 
void respond(void);                            // respond to request routine 
void fuel_calc(void);                          // fuel usage calculation 

void get_status(void);                         // opcode E01 
void approve(void);                            // opcode E02 
void sale_data(void);                          // opcode E03 
void halt(void);                               // opcode E04 
//void set_ppl(void);                          // opcode E05 

void pump_pause(void);                         // NEW opcode E10
void pump_continue(void);                      // NEW opcode E11     

void pump_type(void);                          // NEW opcode E20

void read_serial_no(void);                     // NEW opcode E23
void set_serial_no(void);                      // NEW opcode E24
 

void set_id(void);                             // NEW opcode E97
void read_id();                                // NEW opcode E98
void software(void);                           // opcode E99 .... software version 

void reset_pulse(void);

void watchdog(void);                           // reset watchdog timer   

//void send_TAG_data(void);  
void send_NIU_data(void);
                                        
void clear_SBUF(void);

void over_ride(void);                          // over ride transactions 
void clear_ALL(void);

void clear_power_fail(void);                   // clear stored POWER FAIL DATA


// ******************* UNION DECLARATION ***********************

         // name of union is "pulser_count" .... but name is never used ......
data union pulse_count                   // union declaration  ... different variable types start at same memory location 
{
   unsigned long int pulse;              // used as pulse counter
   unsigned char FLOW[4];               // used to store individual bytes (of "long int pulse")
  
   
}  COUNTER;                             // ACTUAL NAME TO BE USED ... ie. COUNTER.pulse.... or COUNTER.FLOW[x]....



// ................................... VARIABLES .......................................... 

// "data" = 1st 128 bytes of INTERNAL  RAM
unsigned int  data /*x,*/pulser_ratio;               
unsigned char data nozzle_id,nozzle_id1,nozzle_id2;
unsigned char data DCM_APPROVED,first_read; 
unsigned char data /*DATA_GOOD,*/stat;  
unsigned char data TAG_BUFFER[60];  
unsigned long idata fuel1,fuel2;
unsigned int  Fuel_Still_Flowing,nozzle_down;
unsigned int  data VCU_PRESENT_Last;
unsigned long data new_tank_cap_percentage = 0;
unsigned long data new_tank_cap = 0;	


// "idata" = 2nd 128 bytes of INTERNAL RAM
unsigned char idata SBUF_BUFFER[65];         //JP changed from 83 00/09/22
unsigned char idata timeout,nozzle_timeout,a,b,c,LRC;
unsigned char idata BAUDRATE,tag_read,vcu_read;
unsigned char idata ID_LOOP,loop,loop2,buffer_size;
unsigned char idata nozzle_delay;
unsigned char idata vcu_check,CommsTimeout,vcu_tag;
unsigned char idata LITRES[8];          // HO now 8 digits
unsigned char idata LITRE_BUSY;                           

// ------------------------------------------------------------------------ 

 

// ................................ USER INCLUDE FILES .................................... 

#include "93c46.c"          
#include "crc16.c" 
              

// ............................... WATCHDOG TIMER RESET ................................... 
void watchdog(void)
{
     WDG = ~WDG;                         // toggle port pin for DS1232
     if((POWER_FAIL == 0)&&(DCM_APPROVED == 1)) // if POWER IS FAILING AND DCM is APPROVED !!
     {
        MOTOR = 1;                       //  STOP FUELLING !!
        EA = 0;                          // disable all interrupts !
        
        write_rom(25,COUNTER.FLOW[0]);   // stored MSD
        write_rom(26,COUNTER.FLOW[1]);              
        write_rom(27,COUNTER.FLOW[2]);                 
        write_rom(28,COUNTER.FLOW[3]);   // stored LSD

        fuel2 = 0;

        while(fuel2 < 100000)            // .... a long delay
        {
           WDG = ~WDG;                   // toggle port pin for DS1232
           fuel2++;
        }
        
        while(1);                        // cause a WATCH_DOG TIMEOUT... if only BROWNOUT       
     }
}

// ............................... INTERRUPT INITIALIZATION .............................. 
void int_init(void)
{
	IR_LED		=	1;        // P0_0 - Infra Red LED - TAG READER
	RED_LED		=	1;        // P0_1 - RED_LED - TAG_READER
	OSC_CONTROL	=	1;        // P0_3 - OSC. Control - TAG READER	
	COMMS_TX 	=	1;        // P0_6 - 75176 pin 2 
	MOTOR    	=	1;        // P0_7 - relay   

	EA=0;            // disable interrupts   

	IE0=0;           // clear all interrupt flags 
	IE1=0;

	PX1=1;           // INT1 priority        
	IT1=1;           // INT1 edge triggered  
	EX1=1;           // enable INT1          

	COMMST = 1; 
	COMMSS = 1; 
}



// .............................. SYSTEM INITIALISE .......................................... 
void sys_init(void)
{
	watchdog();     
	Fuel_Still_Flowing=0;
    // P0=P1=P2=P3=0xff;           // all port pins HI 
    LED = 0;
    NZ_2WIRE_TX = 0;            // keep nozzle RS485 in RX ....

   	OSC_CONTROL = 1;            // TAG osc. OFF (inverted - 74HC14)
    RED_LED     = 0;            // TAG osc. OFF (double inverted - 74HC14 + MOSFET)
    IR_LED      = 1;            // TAG osc. OFF (inverted - 74HC14)

	COMMS_TX=0;                 // disable TX of 75176 

    stat=1;                     // pump idle 
    a=0;                        // reset nozzle lift variable 
    reset_pulse();
    for(loop=0;loop<=5;loop++)
    {
    	LED = 1;
        RED_LED = 1;
        timer(5000);
        LED = 0;
        RED_LED = 0;
        timer(5000);
     }

     SYSTEM_COMMS = 0;           // switch 4066 channel OFF
     NOZZLE_COMMS = 0;           // switch 4066 channel OFF
     TAG_COMMS    = 0;           // switch 4066 channel OFF

     timer(1000);
     pulser_ratio = ((read_rom(3) * 10000) + (read_rom(4) * 1000) + (read_rom(5) * 100) + (read_rom(6) * 10) + read_rom(7));
     timer(1000);
     nozzle_delay = read_rom(9);   // ON / OFF delay
     timer(1000);
     if(nozzle_delay == 0)
     {
        write_rom(9,10);           // force a value if zero
        timer(1000);
     }
     nozzle_id = read_rom(10);     // DEVICE ID 
     
     nozzle_id1 = nozzle_id / 10;     // MSD
     nozzle_id2 = nozzle_id % 10;     // LSD 
     
     watchdog();
     
     nozzle_id1 += 0x30;
     nozzle_id2 += 0x30;
      
// !!! EEPROM LOCATIONS ... !!! DO NOT USE LOCATIONS ...
// !!! ... 0,8,16,24,32,40,48,56,64,72,80,88,96,104,112,120,128 !!!
//
//  1 = PUMP TYPE
//  2 = Indicator that serial number has been set
//  3 = PULSER RATIO - 1000s 
//  4 = PULSER RATIO - 100s 
//  5 = PULSER RATIO - 10s
//  6 = PULSER RATIO - 1s
//  7 = PULSER RATIO - 0.1s
//  8 = !!!!!!!!!!!!!!!!!!!!!!!!! DO NOT USE !!!!!!!!!!!!!!!!!!!!!!
//  9 = nozzle delay 
// 10 = device ID .... in HEX
// 11 = SERIAL No: - MSB
// 12 = SERIAL No:
// 13 = SERIAL No:
// 14 = SERIAL No:
// 15 = SERIAL No:
// 16 = !!!!!!!!!!!!!!!!!!!!!!!!! DO NOT USE !!!!!!!!!!!!!!!!!!!!!!
// 17 = SERIAL No:
// 18 = SERIAL No:
// 19 = SERIAL No:
// 20 = SERIAL No: - LSB
// 21 = POLL VERIFONE IF VCU is detected ??
// 22 =
// 23 = 
// 24 = !!!!!!!!!!!!!!!!!!!!!!!!! DO NOT USE !!!!!!!!!!!!!!!!!!!!!!
// 25 = "pulse" byte1 MSD ...... stored power fail value
// 26 = "pulse" byte2     ...... stored power fail value
// 27 = "pulse" byte3     ...... stored power fail value
// 28 = "pulse" byte4 LSD    ...... stored power fail value
 

	//Commstimeout timer
	T2CON = 0x04;
	//T2MOD = 0x00;
	RCAP2H = 0x2c;
	RCAP2L = 0x11;
	ET2 = 1;
}



// ................................... INITIALISE COMMS ........................................ 
void comms_init(void)
{
     // initialising UART - serial comms 
	 PCON=0;         // SMOD=0 in PCON register
	 SCON=0x50;      // 8bit uart in mode 1 
	 TMOD |= 0x21 ;  // timer 1 set to 8 bit auto-reload (mode2) + TIMER0 = 16 bit
	 TR1=1;          // timer 1 set to on 

     baud96;         // set baudrate to 9600
     BAUDRATE = 96;  // indicator only

	 TI=0;           // reset transmit interupt 
     RI=0;           // reset receive interrupt 
	 TXD=1;          // set TX pin HI 
	 RXD=1;          // set RX pin HI 
     REN=1;          // enable comms recieve 
	
}

// ....................... INT0 interrupt to switch 4066 channel................................. 
void system_comms_interrupt(void)    interrupt 0  // INT0 interrupt
{
   EX0 = 0;              // disable further interrupt
   BAUDRATE = 96;        // indicator only
   baud96;               // change baudrate to 9600
   SYSTEM_COMMS = 1;     // switch 4066 channel ON
   NOZZLE_COMMS = 0;
   TAG_COMMS = 0;

   
}

// .................... TIMER0 interrupt to flash RED LED on TAG Reader ........................
void flashing_LED(void) interrupt 1     
{
   RED_LED = ~RED_LED;          // toggle LED ON / OFF
   OSC_CONTROL = 0;             // switch TAG osc. ON
}

// .............................. PULSER INTERRUPT ..............................................
void pulser_int(void)   interrupt 2      // INT1 Interrupt 
{                                       
   EA=0;                                 // disable all interrupts 
   COUNTER.pulse++;
   Fuel_Still_Flowing = 600;				 // added by Philip Foster 2016/08/25
   //nozzle_timeout=0;
   nozzle_down=0;
   //timer(50);
   EA=1;                                 // enable all interrupts 
}

// ..................................TIMER INTERUPT............................................... 
void timer_int(void)  interrupt 5
{
    ET2 = 0;
    
    CommsTimeout++;    
    COMMST = ~COMMST;
    TF2 = 0;
    
    if((CommsTimeout >= COMMS_TIMEOUT))// && (DCM_APPROVED == 1))
    {
       COMMST = 0;
       MOTOR = 1;                // MOTOR OPEN ... comms timeout
       CommsTimeout = COMMS_TIMEOUT;
    }    
    
    ET2 = 1;
}

// (((((((((((((((((((((((((( SERIAL COMMS INTERUPT ))))))))))))))))))))) 
void rx_interrupt(void) interrupt 4
{
	unsigned int data y,x;   
  	unsigned char tag_type,DATA_GOOD;
	unsigned char idata VCU_BUFFER[9];       // do comparison if another tagged vehicle  
	unsigned char shift = 0, i;
	

   	ES=0;                                     // disable comms interrupt 
	
   	DATA_GOOD = 0;                            // clear variable
   	buffer_size = 8;                          // default of 8 characters
   	
   	if((BAUDRATE == 24)&&(TH1 == 0xf4))                        // TAGs and VCUs ......
   	{
		// ............................. PAN TAG or VCU ......................................

    	x=0;
    	RI=0;
    	REN=1;
		
    	for(loop=0;loop<=64;loop++)         // receive characters      //JP changed from 82 00/09/22
    	{
	   		RED_LED=0;
       		RI = 0;                       	// reset RX interrupt
	   		REN = 1;                      	// enable RX  

	       	if(VCU_PRESENT==1) y=40000;		// if a VCU then set a long timeout ( for AMT )
            if(TAG_PRESENT==1) y=3000;		// if a TAG then set short timeout
            	
	   		while((RI==0)&&(x < y))    		// wait until RX is complete
	   		{
		  		x++;                    	// or timeout
	       		watchdog();
	    	}
        			
	    	SBUF_BUFFER[loop]=SBUF;        	// read SBUF into "SBUF_BUFFER"
	    	RI = 0;                       	// reset RX interupt

        	if(x >= y)  					// if there was a timeout
        	{       
	       		for(loop=0;loop<=64;loop++) //JP changed from 82 00/09/22
       	  		{
           			SBUF_BUFFER[loop] = 0;	// clear buffer contents
           		}
           		break;						// and break out of loop
        	}
	    	x=0;                          	// reset timeout variable
       	}
	
   		ES = 0;
   		TI = 0;
   		RI = 0;
   		SYSTEM_COMMS = 1;
   		OSC_CONTROL = 1;            // TAG osc. OFF (inverted - 74HC14)   
   		TAG_COMMS = 0;
   		NOZZLE_COMMS = 0; 
   		LRC = 0;                    // clear variable

		for(loop=38;loop<=64;loop++)		// search for correct buffer     //JP changed from 82 00/09/22
       	{
			if((SBUF_BUFFER[loop]==0x20)&&(SBUF_BUFFER[loop+1]==0x0d)&&(SBUF_BUFFER[loop+2]==0x0a))
       	   	{
       	    	tag_type = loop - 39;   // read the "P" or "STX"
       	   		x=loop-38;		// starting point of buffer to read
       	   		watchdog();
       	        loop=83;
       	       	break;                  // once starting point found set variable and break out of loop
       	   	}
       	        
       	    if((loop>=57)&&(SBUF_BUFFER[loop]==0x15)&&(SBUF_BUFFER[loop+1]==0x0d)&&(SBUF_BUFFER[loop+2]==0x0a))
       	    {
       	    	tag_type = loop-57;
       	        x=loop-57;
       	        watchdog();
       	        loop=83;
       	        break;
			}
       	    
			watchdog();
       	        	
       	}
       
        if((SBUF_BUFFER[x+57]==0x15)&&(SBUF_BUFFER[x+58]==0x0d)&&(SBUF_BUFFER[x+59]==0x0a))
        {   
        	   	        						// VKU detected so do CRC
            DATA_GOOD=0;
            for(loop=0;loop<=59;loop++)
            {
            	TAG_BUFFER[loop]=SBUF_BUFFER[loop+x];	// put correct length in buffer
            }
            watchdog();
            for(loop=0;loop<=59;loop++)
            {
            	SBUF_BUFFER[loop]=TAG_BUFFER[loop];		// write back to buffer
            }
                
			crc16(0,56);									// do crc
            	
            if (CRC16==0) DATA_GOOD=1;
		}

		if((SBUF_BUFFER[x+38]==0x20)&&(SBUF_BUFFER[x+39]==0x0d)&&(SBUF_BUFFER[x+40]==0x0a))
        {
       		for(loop=0;loop<=41;loop++)
       		{
       	   		TAG_BUFFER[loop]=SBUF_BUFFER[loop+x];	// store correct buffer length
       		}	
	       
       		watchdog();
	       
       		for(loop=0;loop<=41;loop++)
       		{
           		SBUF_BUFFER[loop]=TAG_BUFFER[loop];	// write back to sbuf buffer
       		}	

            LRC=0;			
                
            if(tag_type == 'P')
            {
            	LRC = (SBUF_BUFFER[1] ^ SBUF_BUFFER[2]);   
            }
            else LRC = ((SBUF_BUFFER[1] - 0x30) ^ (SBUF_BUFFER[2] - 0x30));           // AS PER "BUG" IN VCU CODE !!!!
                
			for(loop = 3; loop <=37;loop++)
       		{
	   			watchdog();
	   			LRC = (LRC ^ SBUF_BUFFER[loop]);
           		watchdog();
			}
       		
       		DATA_GOOD = 0;
      
				if((SBUF_BUFFER[41] == LRC) 
       				&&(SBUF_BUFFER[40] == 0x0a) 
					&& (SBUF_BUFFER[39] == 0x0d) 
					&& (SBUF_BUFFER[38] == 0x20) 
       	       		&& (x < 1000)
					
        			&&(SBUF_BUFFER[1] >= 0x30) && (SBUF_BUFFER[1] <= 0x7a)          // BIN       // CHECK WAS FROM 0x30 -> 0x39 !!!!!!!!!!!!
        			&&(SBUF_BUFFER[2] >= 0x30) && (SBUF_BUFFER[2] <= 0x7a)
           			&&(SBUF_BUFFER[3] >= 0x30) && (SBUF_BUFFER[3] <= 0x7a)
        			&&(SBUF_BUFFER[4] >= 0x30) && (SBUF_BUFFER[4] <= 0x7a)
        			&&(SBUF_BUFFER[5] >= 0x30) && (SBUF_BUFFER[5] <= 0x7a)
       				&&(SBUF_BUFFER[6] >= 0x30) && (SBUF_BUFFER[6] <= 0x7a)
        
        			&&(SBUF_BUFFER[7] >= 0x30) && (SBUF_BUFFER[7] <= 0x7a)          // SITE ID       
        			&&(SBUF_BUFFER[8] >= 0x30) && (SBUF_BUFFER[8] <= 0x7a)
        			&&(SBUF_BUFFER[9] >= 0x30) && (SBUF_BUFFER[9] <= 0x7a)
        			&&(SBUF_BUFFER[10] >= 0x30) && (SBUF_BUFFER[10] <= 0x7a)
        			&&(SBUF_BUFFER[11] >= 0x30) && (SBUF_BUFFER[11] <= 0x7a)
        
        			&&(SBUF_BUFFER[12] >= 0x30) && (SBUF_BUFFER[12] <= 0x7a)        // TAG NUMBER       
        			&&(SBUF_BUFFER[13] >= 0x30) && (SBUF_BUFFER[13] <= 0x7a)
        			&&(SBUF_BUFFER[14] >= 0x30) && (SBUF_BUFFER[14] <= 0x7a)
        			&&(SBUF_BUFFER[15] >= 0x30) && (SBUF_BUFFER[15] <= 0x7a)
        			&&(SBUF_BUFFER[16] >= 0x30) && (SBUF_BUFFER[16] <= 0x7a)
                
        			&&(SBUF_BUFFER[23] >= 0x30) && (SBUF_BUFFER[23] <= 0x7a)        // Km / Hr ....        
        			&&(SBUF_BUFFER[24] >= 0x30) && (SBUF_BUFFER[24] <= 0x7a)
        			&&(SBUF_BUFFER[25] >= 0x30) && (SBUF_BUFFER[25] <= 0x7a)
        			&&(SBUF_BUFFER[26] >= 0x30) && (SBUF_BUFFER[26] <= 0x7a)
        			&&(SBUF_BUFFER[27] >= 0x30) && (SBUF_BUFFER[27] <= 0x7a)
        			&&(SBUF_BUFFER[28] >= 0x30) && (SBUF_BUFFER[28] <= 0x7a)
        			&&(SBUF_BUFFER[29] >= 0x30) && (SBUF_BUFFER[29] <= 0x7a)) 
					{
						DATA_GOOD = 1;
					}
		}        	
	
		if(DATA_GOOD == 1)
		{
       		watchdog();
            LED=1;
			timer(10000);
			LED=0;
			
			for(loop=0;loop<=53;loop++)
            {
            	TAG_BUFFER[loop]=SBUF_BUFFER[loop];
            }    

       		if(TAG_BUFFER[0] == 0x02) vcu_read = 1;      // VCU read 
	      	if(TAG_BUFFER[0] == 0x50) tag_read = 2;      // tag read

       		timeout = 0;
       		nozzle_timeout = 0;

			/* shift = 12;
			if(TAG_BUFFER[shift + 0] == 0x30)//0
			{
				if(TAG_BUFFER[shift + 1] == 0x34)//4
				{
					if(TAG_BUFFER[shift + 2] == 0x35)//5
					{
						if(TAG_BUFFER[shift + 3] == 0x37)//7
						{
							//while(1);
						}
					}
				}
			}   */

			shift = 12;
			new_tank_cap = 0;

			for(i=0;i<5;i++)   // convert to HEX
			{
				if(TAG_BUFFER[shift + i] == 0x30)
				{
					TAG_BUFFER[shift + i] = 0x00;				
				}
				else
				{
					switch(TAG_BUFFER[shift + i])
					{
						case 0x31:TAG_BUFFER[shift + i] = 0x01;break;
						case 0x32:TAG_BUFFER[shift + i] = 0x02;break;
						case 0x33:TAG_BUFFER[shift + i] = 0x03;break;
						case 0x34:TAG_BUFFER[shift + i] = 0x04;break;

						case 0x35:TAG_BUFFER[shift + i] = 0x05;break;
						case 0x36:TAG_BUFFER[shift + i] = 0x06;break;
						case 0x37:TAG_BUFFER[shift + i] = 0x07;break;
						case 0x38:TAG_BUFFER[shift + i] = 0x08;break;
						case 0x39:TAG_BUFFER[shift + i] = 0x09;break;
						
						case 'A':TAG_BUFFER[shift + i] = 0x0A;break;
						case 'B':TAG_BUFFER[shift + i] = 0x0B;break;
						case 'C':TAG_BUFFER[shift + i] = 0x0C;break;
						case 'D':TAG_BUFFER[shift + i] = 0x0D;break;
						case 'E':TAG_BUFFER[shift + i] = 0x0E;break;
						case 'F':TAG_BUFFER[shift + i] = 0x0F;break;
					}
				}
			}									
				/////////								
				
			new_tank_cap =  TAG_BUFFER[shift + 0] * (16*16*16);
			///new_tank_cap =  new_tank_cap + TAG_BUFFER[shift - 1] * (16*16*16*16);
			new_tank_cap = new_tank_cap + (TAG_BUFFER[shift + 1] * (16*16));
			new_tank_cap = new_tank_cap + (TAG_BUFFER[shift + 2] * (16));
			new_tank_cap = new_tank_cap + (TAG_BUFFER[shift + 3] * (1));

			new_tank_cap = (new_tank_cap*100)* .85;
			new_tank_cap_percentage = new_tank_cap;

			TAG_BUFFER[shift + 3] = new_tank_cap % 16;
			new_tank_cap = new_tank_cap / 16;
			
			TAG_BUFFER[shift + 2] = new_tank_cap % 16;
			new_tank_cap = new_tank_cap / 16;
			
			TAG_BUFFER[shift + 1] = new_tank_cap % 16;
			new_tank_cap = new_tank_cap / 16;
			
			TAG_BUFFER[shift + 0] = new_tank_cap % 16;
			new_tank_cap = new_tank_cap / 16;			

			for(i=0;i<5;i++)
			{
				if(TAG_BUFFER[shift + i] == 0)
				{
					TAG_BUFFER[shift + i] = 0x30;				
				}
				else
				{
					switch(TAG_BUFFER[shift + i])
					{
						case 1:TAG_BUFFER[shift + i] = 0x31;break;
						case 2:TAG_BUFFER[shift + i] = 0x32;break;
						case 3:TAG_BUFFER[shift + i] = 0x33;break;
						case 4:TAG_BUFFER[shift + i] = 0x34;break;

						case 5:TAG_BUFFER[shift + i] = 0x35;break;
						case 6:TAG_BUFFER[shift + i] = 0x36;break;
						case 7:TAG_BUFFER[shift + i] = 0x37;break;
						case 8:TAG_BUFFER[shift + i] = 0x38;break;
						case 9:TAG_BUFFER[shift + i] = 0x39;break;
						
						case 0x0A:TAG_BUFFER[shift + i] = 'A';break;
						case 0x0B:TAG_BUFFER[shift + i] = 'B';break;
						case 0x0C:TAG_BUFFER[shift + i] = 'C';break;
						case 0x0D:TAG_BUFFER[shift + i] = 'D';break;
						case 0x0E:TAG_BUFFER[shift + i] = 'E';break;
						case 0x0F:TAG_BUFFER[shift + i] = 'F';break;
					}
				}
			}			

		  
  			shift = 31;
			if(TAG_BUFFER[shift + 0] == 0x34)
            	{
                	if(TAG_BUFFER[shift + 1] == 0x32)
                	{
                    	if(TAG_BUFFER[shift + 2] == 0x35)
                    	{
                        	if(TAG_BUFFER[shift + 3] == 0x30)
                        	{
                            	if(TAG_BUFFER[shift + 4] == 0x32)
                            	{
                                	if(TAG_BUFFER[shift + 5] == 0x38)
                                	{
                                    	if(TAG_BUFFER[shift + 6] == 0x30)
                                    	{
                                        	//while(1);
                                    	}
                                	}
                            	}
                        	}
                    	}
                	}
            	}  

       	 	if((DCM_APPROVED == 0) && (vcu_read == 1))  // save number before approved ....
        	{
       			if(first_read == 0) 
       			{
   					for(loop=30;loop<=37;loop++)
       				{
           				VCU_BUFFER[loop - 30] = TAG_BUFFER[loop];
       					watchdog();
   					}
                   	first_read  = 1;
   				}
           	}

            if(first_read == 1)
   	  		{ 
   				vcu_check = 0;
         
	       		for(loop=30;loop<=37;loop++)
   		 		{
   					if(TAG_BUFFER[loop] == VCU_BUFFER[loop - 30]) vcu_check++;
   					watchdog();
	       		}

   				if((vcu_check >= 8) && (DCM_APPROVED == 1)) // do a comparison of last VCU before switching on again
   				{
       				watchdog();
  		   			MOTOR = 0;                              // switch motor back on
       				//IE1 = 0;
       				//EX1 = 1;                                // allow interrupt  
           		}
   			}

   	 		ET0 = 0;
      		RED_LED = 1;
   			OSC_CONTROL = 1;            // TAG osc. OFF (inverted - 74HC14)
		}
	
  		timer(10); 	
	}

    // ............................... COMMS FROM A HOST SYSTEM ... ie: S.C.M. PANEL / PC etc. ....
	else if(BAUDRATE == 96)
   	{
    	buffer_size = 8;

		if(SBUF == 0x02)                        // STX
     	{
     		COMMSS = ~COMMSS;
     		CommsTimeout = 0;                         //assume valid comms...........     		   
       		SBUF_BUFFER[0] = SBUF;
       		//watchdog();
       		x=0;

       		for(loop=1;loop<=buffer_size;loop++)  // receive characters
       		{
	       		RI = 0;                       // reset RX interrupt
	       		REN = 1;                      // enable RX
               	watchdog();
	       		while((RI==0)&&(x < 1000))    // wait until RX is complete
	       		{
		  			x++;                       // or timeout
	          		//watchdog();
	       		}

	       		SBUF_BUFFER[loop]=SBUF;       // read SBUF into "SBUF_BUFFER"
	       		RI = 0;                       // reset RX interupt
	       		x=0;                          // reset timeout variable
	       		watchdog();

				if(loop == 4) 
	       		{
                	if((SBUF_BUFFER[3] == 0x30)&&(SBUF_BUFFER[4] == 0x32))      // command "E02"
		 			{
		    			buffer_size = 25;
		 			}
		 			else if((SBUF_BUFFER[3] == 0x30)&&(SBUF_BUFFER[4] == 0x35)) // command "E05"
		 			{
		    			buffer_size = 24;
		 			}
					else if((SBUF_BUFFER[3] == 0x32)&&(SBUF_BUFFER[4] == 0x30)) // command "E20"
		 			{
		    			buffer_size = 14;
		 			}
 					else if((SBUF_BUFFER[3] == 0x32)&&(SBUF_BUFFER[4] == 0x34)) // command "E24"
        	 		{
		    			buffer_size = 17;
		 			}
					else if((SBUF_BUFFER[3] == 0x34)&&(SBUF_BUFFER[4] == 0x31)) // command "E41"
		 			{
		    			buffer_size = 9;
		 			}
					else if((SBUF_BUFFER[3] == 0x34)&&(SBUF_BUFFER[4] == 0x32)) // command "E42"
		 			{
		    			buffer_size = 9;
		 			}
					else if((SBUF_BUFFER[3] == 0x34)&&(SBUF_BUFFER[4] == 0x34)) // command "E44"
		 			{
		   	 			buffer_size = 9;
		 			}
					else if((SBUF_BUFFER[3] == 0x34)&&(SBUF_BUFFER[4] == 0x35)) // command "E45"
		 			{
		    			buffer_size = 9;
		 			}
					else if((SBUF_BUFFER[3] == 0x39)&&(SBUF_BUFFER[4] == 0x31)) // command "E91"
		 			{
		    			buffer_size = 11;
		 			}
					else if((SBUF_BUFFER[3] == 0x39)&&(SBUF_BUFFER[4] == 0x33)) // command "E93"
		 			{
		    			buffer_size = 18;
		 			}
                    else if((SBUF_BUFFER[3] == 0x39)&&(SBUF_BUFFER[4] == 0x34)) // command "E94"
		 			{
		    			buffer_size = 16;
		 			}

                    watchdog();
               	}

       		}

			// check LRC of recieved message ......

       		LRC = 0;                             // clear variable

       		for(loop = 2;loop <=(buffer_size -1);loop++)
       		{
	   			watchdog();
	   			LRC = (LRC ^ SBUF_BUFFER[loop]);
            }
                        
     		if(buffer_size == 5)
       		{
          		LRC = (SBUF_BUFFER[1] ^ SBUF_BUFFER[2]);
          		LRC = (LRC ^ SBUF_BUFFER[3]);
          		if((SBUF_BUFFER[2] == nozzle_id) && (SBUF_BUFFER[5] == LRC)) DATA_GOOD = 1;
			}

       		if((buffer_size == 8)&&(SBUF_BUFFER[3] == 0x34)&&(SBUF_BUFFER[4] == 0x30)) // NIU LRC is all characters ...
       		{
         		LRC = 0;
          		for(loop=0;loop<=7;loop++)
          		{
             		LRC = (LRC ^ SBUF_BUFFER[loop]);
          		}
          
          		//if((SBUF_BUFFER[6] == nozzle_id) && (SBUF_BUFFER[8] == LRC)) DATA_GOOD = 1;
          		if((SBUF_BUFFER[5] == nozzle_id1)&&(SBUF_BUFFER[6] == nozzle_id2) && (SBUF_BUFFER[8] == LRC)) 
          		{ 
          			DATA_GOOD = 1;
                }
                                
       		}        

			//if(SBUF_BUFFER[6] == (nozzle_id + 0x30))
       		if((SBUF_BUFFER[5] == nozzle_id1)&&(SBUF_BUFFER[6] == nozzle_id2)&&(DATA_GOOD == 0))
       		{
	 			if((buffer_size == 8)  && (SBUF_BUFFER[8]  == LRC)) DATA_GOOD = 1;
         			else if((buffer_size == 9)  && (SBUF_BUFFER[9]  == LRC)) DATA_GOOD = 1;
         			else if((buffer_size == 11) && (SBUF_BUFFER[11] == LRC)) DATA_GOOD = 1;
         			else if((buffer_size == 14) && (SBUF_BUFFER[14] == LRC)) DATA_GOOD = 1;
         			else if((buffer_size == 16) && (SBUF_BUFFER[16] == LRC)) DATA_GOOD = 1;
         			else if((buffer_size == 17) && (SBUF_BUFFER[17] == LRC)) DATA_GOOD = 1;
         			else if((buffer_size == 18) && (SBUF_BUFFER[18] == LRC)) DATA_GOOD = 1;
	 			    else if((buffer_size == 24) && (SBUF_BUFFER[24] == LRC)) DATA_GOOD = 1;
	 			    else if((buffer_size == 25) && (SBUF_BUFFER[25] == LRC)) DATA_GOOD = 1;	 
       		}

       		if((SBUF_BUFFER[3] == 0x39) && (SBUF_BUFFER[4] == 0x37) && (SBUF_BUFFER[8] == LRC)) read_id();     // opcode E97 - read ID
       			else if((SBUF_BUFFER[3] == 0x39) && (SBUF_BUFFER[4] == 0x38)&&(SBUF_BUFFER[8] == LRC)) set_id(); // opcode E98 - set ID
     	}

		if(DATA_GOOD == 1) respond();
   		DATA_GOOD = 0;
   
    }

   	TI  = 0;
   	RI  = 0;
   	REN = 1;
   	ES  = 0;
}



// ...................................respond routine..............................................
void respond(void)                     
{
   ES=0;							// disable comms interrupt 
   LED = 1;							// buzzer ON

   watchdog();						// reset watchdog timer


   if(SBUF_BUFFER[3] == 0x30)		// opcode E0x....
   {
      switch(SBUF_BUFFER[4])
      {
      	case(0x30):
                   clear_power_fail();		// opcode E00       ... ACK of power fail data download
                   break;
                   
	 	case(0x31):
		   get_status();          			// opcode E01
		   break;

	 	case(0x32):
  	       COUNTER.FLOW[0] = read_rom(25);        // read from EEPROM - MSD ....
           COUNTER.FLOW[1] = read_rom(26);        // read from EEPROM
           COUNTER.FLOW[2] = read_rom(27);        // read from EEPROM
           COUNTER.FLOW[3] = read_rom(28);        // read from EEPROM - LSD ....
                   
		   if((stat == 2) && (COUNTER.pulse == 0)) approve();  // opcode E02
     		   else if((stat != 2)&&(COUNTER.pulse == 0)) reset_pulse();    // if approve command is received + nozzle down  
		   break;

	 	case(0x33):
		   sale_data();           // opcode E03
		   break;

	 	case(0x34):
		   halt();                // opcode E04
		   break;

     	/*	 case(0x35):
		   set_ppl();             // opcode E05
		   break;
      

         case(0x37):
		   electronic_totals();   // opcode E07
		   break;
      	*/

      }
   }

   else if(SBUF_BUFFER[3] == 0x31)        // opcode E1x....
   {
       switch(SBUF_BUFFER[4])
       {
	    case(0x30):
		      pump_pause();      // NEW opcode E10
		      break;

	    case(0x31):
		      pump_continue();   // NEW opcode E11
		      break;

	/*    case(0x32):
		      fast_ON();         // NEW opcode E12
		      break;

	    case(0x33):
		      fast_OFF();         // NEW opcode E13
		      break;

	    case(0x34):
		      slow_ON();          // NEW opcode E14
		      break;

	    case(0x35):
		      slow_OFF();         // NEW opcode E15
		      break;


	    case(0x36):
		      LITRE_2DP();         // NEW opcode E16
		      break;

	    case(0x37):
		      LITRE_3DP();         // NEW opcode E17
		      break;

	    case(0x38):
		      PPL_DP_ON();         // NEW opcode E18
		      break;

	    case(0x39):
		      PPL_DP_OFF();        // NEW opcode E19
		      break;
          */

       }
   }

   else if(SBUF_BUFFER[3] == 0x32)        // opcode E2x....
   {
       switch(SBUF_BUFFER[4])
       {
	    case(0x30):
		      pump_type();      // NEW opcode E20 - setup pump type + pulser ratio
		      break;

          /*  case(0x31):                 // NEW opcode E21 - SET RAND LIMIT ON
                      set_RAND_LIMIT_ON();
                      break;

            case(0x32):                 // NEW opcode E22 - SET RAND LIMIT OFF
                      set_RAND_LIMIT_OFF();
                      break;

            */ 
		case(0x33):
		      read_serial_no();      // NEW opcode E23 - read device serial number
		      break;

        case(0x34):
		      set_serial_no();       // NEW opcode E24 - set device serial number
		      break;
	   }
   }

   else if(SBUF_BUFFER[3] == 0x34)        // opcode E4x....
   {
	switch(SBUF_BUFFER[4])
       {
	    case(0x30):
		      send_NIU_data();    // NEW opcode E40 - NIU data request
		      break;

        case(0x31):
		      write_rom(9,SBUF_BUFFER[7]);    // NEW opcode E41 - nozzle delay time
                      nozzle_delay = SBUF_BUFFER[7];  // new value
		      break;

        case(0x32):                               // NEW opcode E42 - read nozzle delay
                       SBUF_BUFFER[0] = 0x02;                // STX             // header 
                       SBUF_BUFFER[1] = 0x32;                // SEQ CHAR        // header 
                       SBUF_BUFFER[2] = 0x45;                // "E" 
                       SBUF_BUFFER[3] = 0x34;                // "4"
                       SBUF_BUFFER[4] = 0x32;
                       SBUF_BUFFER[5] = nozzle_id1;
                       SBUF_BUFFER[6] = nozzle_id2;          //nozzle_id   //                 // pump number 
                       SBUF_BUFFER[7] = read_rom(9);         // nozzle timeout value
                       SBUF_BUFFER[8] = 0x03;                // ETX             // trailer 
                     
                       LRC = SBUF_BUFFER[2] ^ SBUF_BUFFER[3];
                       for(loop=4;loop<=7;loop++)
                       {
                          LRC = (LRC ^ SBUF_BUFFER[loop]);
                       }
                       SBUF_BUFFER[9] = LRC;
    
                       COMMS_TX=1;                         // enable transmit 
                       timer(100);

                       for(loop=0;loop<=9;loop++)
                       {
                          TI=0;
                          SBUF=SBUF_BUFFER[loop];
                          while(!TI);
                          watchdog();                          // watchdog 
                       }

                       COMMS_TX=0;                 // disable transmit 
                       break;

		case(0x33):                               // NEW opcode E43 - read pulser ratio
                       SBUF_BUFFER[0] = 0x02;                // STX             // header 
                       SBUF_BUFFER[1] = 0x32;                // SEQ CHAR        // header 
                       SBUF_BUFFER[2] = 0x45;                // "E" 
                       SBUF_BUFFER[3] = 0x34;                // "4"
                       SBUF_BUFFER[4] = 0x33;
                       SBUF_BUFFER[5] = nozzle_id1;
                       SBUF_BUFFER[6] = nozzle_id2;          //nozzle_id   //                 // pump number 
                       SBUF_BUFFER[7] = (read_rom(3) + 0x30);         // pulser ration - 1000s
                       SBUF_BUFFER[8] = (read_rom(4) + 0x30);         // pulser ration - 100s
                       SBUF_BUFFER[9] = (read_rom(5) + 0x30);         // pulser ration - 10s
                       SBUF_BUFFER[10] = (read_rom(6) + 0x30);         // pulser ration - 1s
                       SBUF_BUFFER[11] =(read_rom(7) + 0x30);         // pulser ration - 0.1s

                       SBUF_BUFFER[12] = 0x03;                // ETX             // trailer 
                     
                       LRC = SBUF_BUFFER[2] ^ SBUF_BUFFER[3];
                       for(loop=4;loop<=12;loop++)
                       {
                          LRC = (LRC ^ SBUF_BUFFER[loop]);
                       }
                       SBUF_BUFFER[13] = LRC;
    
                       COMMS_TX=1;                         // enable transmit 
                       timer(100);

                       for(loop=0;loop<=13;loop++)
                       {
                          TI=0;
                          SBUF=SBUF_BUFFER[loop];
                          while(!TI);
                          watchdog();                          // watchdog 
                       }

                       COMMS_TX=0;                 // disable transmit 
                       break;

		/*           case(0x34):                               // NEW opcode E44 - set FLEET No REQUIREMENT
                       write_rom(3,SBUF_BUFFER[7]);          // store setting
                       SBUF_BUFFER[0] = 0x02;                // STX             // header 
                       SBUF_BUFFER[1] = 0x32;                // SEQ CHAR        // header 
                       SBUF_BUFFER[2] = 0x45;                // "E" 
                       SBUF_BUFFER[3] = 0x34;                // "4"
                       SBUF_BUFFER[4] = 0x32;
                       SBUF_BUFFER[5] = 0x30;
                       SBUF_BUFFER[6] = nozzle_id + 0x30;    //                 // pump number 
                       SBUF_BUFFER[7] = read_rom(3);         // FLEET No requirement
                       SBUF_BUFFER[8] = 0x03;                // ETX             // trailer 
                     
                       LRC = SBUF_BUFFER[2] ^ SBUF_BUFFER[3];
                       for(loop=4;loop<=7;loop++)
                       {
                          LRC = (LRC ^ SBUF_BUFFER[loop]);

                       }
                       SBUF_BUFFER[9] = LRC;
    
                       COMMS_TX=1;                         // enable transmit 
                       timer(100);

                       for(loop=0;loop<=9;loop++)
                       {
                          TI=0;
                          SBUF=SBUF_BUFFER[loop];
                          while(!TI);
                          watchdog();                          // watchdog 
                       }

                       COMMS_TX=0;                 // disable transmit 
                       break;
		*/

		case(0x35):                               // NEW opcode E45 - POLL VERIFONE if VCU detected ?
                       write_rom(21,SBUF_BUFFER[7]);          // store setting
                       SBUF_BUFFER[0] = 0x02;                // STX             // header 
                       SBUF_BUFFER[1] = 0x32;                // SEQ CHAR        // header 
                       SBUF_BUFFER[2] = 0x45;                // "E" 
                       SBUF_BUFFER[3] = 0x34;                // "4"
                       SBUF_BUFFER[4] = 0x32;
                       SBUF_BUFFER[5] = nozzle_id1;
                       SBUF_BUFFER[6] = nozzle_id2;          //nozzle_id   //                 // pump number 
                       SBUF_BUFFER[7] = read_rom(21);        // POLL VERIFONE requirement
                       SBUF_BUFFER[8] = 0x03;                // ETX             // trailer 
                     
                       LRC = SBUF_BUFFER[2] ^ SBUF_BUFFER[3];
                       for(loop=4;loop<=7;loop++)
                       {
                          LRC = (LRC ^ SBUF_BUFFER[loop]);
                       }
                       SBUF_BUFFER[9] = LRC;
    
                       COMMS_TX=1;                         // enable transmit 
                       timer(100);

                       for(loop=0;loop<=9;loop++)
                       {
                          TI=0;
                          SBUF=SBUF_BUFFER[loop];
                          while(!TI);
                          watchdog();                          // watchdog 
                       }

                       COMMS_TX=0;                 // disable transmit 
                       break;
       }
   }

   else if(SBUF_BUFFER[3] == 0x39)        // opcode E9x....
   {
       switch(SBUF_BUFFER[4])
       {
	  /*  case(0x30):
		      check_manual();      // NEW opcode E90 - check for over ride
		      break;

	    case(0x31):
		      fetch_manual();      // NEW opcode E91 - fetch over ride details
		      break;

	    case(0x32):
		      reset_manual();      // NEW opcode E92 - reset over ride details
		      break;

	    case(0x33):
		      set_RTC();         // NEW opcode E93 - set Real Time Clock
		      break;

            case(0x34):
		      set_TOTALISER();    // NEW opcode E93 - set Real Time Clock
		      break;
          */

          

	    case(0x39):
		      software();          // opcode E99 - read software version
		      break;

       }
   }

   clear_SBUF();

   LED = 0;                 // buzzer OFF
  
   SYSTEM_COMMS = 0;           // switch 4066 channel OFF
   NOZZLE_COMMS = 0;           // switch 4066 channel OFF
   TAG_COMMS    = 0;           // switch 4066 channel OFF
	//   DATA_GOOD = 0;              // clear variable
   EX0 = 1;
   ES = 1;                     // enable comms interrupt
}



// ....................... RESET PULSER COUNT ........................... 
void reset_pulse(void)
{
  COUNTER.pulse=0;
  LITRES[7]=LITRES[6]=0;
  LITRES[5]=LITRES[4]=LITRES[3]=LITRES[2]=LITRES[1]=LITRES[0]=0;
}



// ....................... variable TIMER ................................
void timer(unsigned int time)    // delay routine 
{
   //EA=1;
   while(time > 0)
   {
       time--;
       WDG=~WDG;      // toggle watchdog                
   }
}



//..................... E01 ...................................
void get_status(void)                     // opcode E01 
{
   	ES=0;                              // disable comms interrupt 
   	watchdog();
	//   if(stat==5) stat=1;     // if pump  halted - make idle 

	//   if(stat==1) m=0x41;     // "A" .... pump idle  
	//   if(stat==2) m=0x42;     // "B" .... requesting approval 
	//   if(stat==3) m=0x43;     // "C" .... sale in progress - slow flow 
	//   if(stat==4) m=0x44;     // "D" .... no price per litre 
	//   if(stat==5) m=0x45;     // "E" .... pump halted 
	//   if(stat==6) m=0x46;     // "F" .... approved - no valves open 
	//   if(stat==7) m=0x47;     // "G" .... sale in progress - fast flow 

	SBUF_BUFFER[0]=0x02;                  // STX             // header 
   	SBUF_BUFFER[1]=0x32;                  // SEQ CHAR        // header 
   
   	switch(stat)
    {
    	case 1:
        	SBUF_BUFFER[2]=0x41;
            break;
		case 2:
        	SBUF_BUFFER[2]=0x42;
            break;
		case 3:
        	SBUF_BUFFER[2]=0x43;
            break;
		case 5:
        	SBUF_BUFFER[2]=0x41;
            break;
		case 6:
        	SBUF_BUFFER[2]=0x46;
            break;
		case 7:
        	SBUF_BUFFER[2]=0x47;
            break;
	}

	watchdog();               // watchdog 
 
   	SBUF_BUFFER[3]=nozzle_id2;    // pump number - LSD ONLY !!!
   	SBUF_BUFFER[4]=0x03;                  // ETX             // trailer 
   	LRC_calc();              // LRC             // trailer 

   	COMMS_TX=1;                            // transmit enable 
   	timer(10);

	// the folowing commands are for the serial transmission of the data 

   	watchdog();
   
   	for(loop=0;loop<=4;loop++)
   	{
    	TI=0;
     	SBUF=SBUF_BUFFER[loop];
     	while(!TI);
	}

   	TI=0;
   	SBUF=LRC;
   	while(!TI);

 	//  watchdog();                  // watchdog 

   	COMMS_TX=0;                    // disable transmit 
}


//................... E02 ...............................
void approve(void)                     // opcode E02 
{
	DCM_APPROVED = 1;
   	timeout = nozzle_timeout = 0;       // reset timeouts   
   
   	TR0 = 0;                            // flashing LED timer OFF
   	TF0 = 0;                            // clear interrupt flag
   	ET0 = 0;                            // TIMER0 interrupt OFF
    
   	RED_LED = 1;                        // RED LED  ON                 

   	fuel1=0;
   	watchdog();                         // watchdog 
   	reset_pulse();                      // reset pulser count total 

   	SBUF_BUFFER[0]=0x02;                // STX             // header 
   	SBUF_BUFFER[1]=0x32;                // SEQ CHAR        // header 
   	SBUF_BUFFER[2]=0x46;                // "F"             // no valves open 
   	SBUF_BUFFER[3]=nozzle_id2;    		// pump number - LSD ONLY !!!
   	SBUF_BUFFER[4]=0x03;                // ETX             // trailer 
   	LRC_calc();                         // LRC             // trailer 

   	COMMS_TX=1;                         // enable transmit 
   	timer(100);

	// the folowing commands are for the serial transmission of the data 

   	for(loop=0;loop<=4;loop++)
   	{
    	TI=0;
    	SBUF=SBUF_BUFFER[loop];
    	while(!TI);
    	watchdog();                          // watchdog 
	}

   	TI=0;
   	SBUF=LRC;
   	while(!TI);

   	COMMS_TX=0;             // disable transmit 

   	stat=3;               	// approved , in slow flow 
   	b=0;                  	// reset time variable 
   	c=0;                  	// reset time variable 

   	EX1= 0;                	// disable interrupt 


   	MOTOR = 0;      		// relay ON 
   	timer(1000);         	// HCB Changed from 10000 for testing     //JP put back... 00/09/22
   	IE1 = 0;                // clear interrupt flag 
   	fuel1=fuel2;
   	EX1 = 1;                // enable interupt 
}



//..................... E03 .................................
void sale_data(void)                      // opcode E03 
{
   watchdog();                            // watchdog 

   if (LITRE_BUSY == 0) fuel_calc();

   SBUF_BUFFER[0]=0x02;                  // STX             // header 
   SBUF_BUFFER[1]=0x32;                  // SEQ CHAR        // header 

   if((NOZZLE_SW==1)&&(Fuel_Still_Flowing==0)) stat=1;      // pump idle only if nozzle down & no fuel flowing  -  2016/06/22 Philip Foster

   if((stat > 3) &&(stat < 6)) stat=1;    // pump idle 
   if(stat ==6) stat=7;                   // fast flow 
   if(stat > 7) stat=7;                   // fast flow 

   SBUF_BUFFER[2]=0x41;

   if(stat==5) SBUF_BUFFER[2]=0x41;     // if pump  halted - make idle 
   if(stat==1) SBUF_BUFFER[2]=0x41;     // "A" .... pump idle 
   if(stat==3) SBUF_BUFFER[2]=0x43;     // "C" .... sale in progress - slow flow 
   if(stat==7) SBUF_BUFFER[2]=0x47;     // "G" .... sale in progress - fast flow 

   //SBUF_BUFFER[3]=0x31;                 // "1"             // PRD 
   SBUF_BUFFER[3]=nozzle_id2;

   if(LITRES[7] > 9) LITRES[7] = 0; // if digit blanked out
   if(LITRES[6] > 9) LITRES[6] = 0; // if digit blanked out
   if(LITRES[5] > 9) LITRES[5] = 0; // if digit blanked out
   if(LITRES[4] > 9) LITRES[4] = 0; // if digit blanked out
   if(LITRES[3] > 9) LITRES[3] = 0; // if digit blanked out


   SBUF_BUFFER[4]  = LITRES[7] + 0x30;
   SBUF_BUFFER[5]  = LITRES[6] + 0x30;
   SBUF_BUFFER[6]  = LITRES[5] + 0x30;
   SBUF_BUFFER[7]  = LITRES[4] + 0x30;
   SBUF_BUFFER[8]  = LITRES[3] + 0x30;
   SBUF_BUFFER[9]  = LITRES[2] + 0x30;
   SBUF_BUFFER[10] = LITRES[1] + 0x30;
   SBUF_BUFFER[11] = LITRES[0] + 0x30;


   SBUF_BUFFER[12] = LITRES[7] + 0x30;
   SBUF_BUFFER[13] = LITRES[6] + 0x30;
   SBUF_BUFFER[14] = LITRES[5] + 0x30;
   SBUF_BUFFER[15] = LITRES[4] + 0x30;
   SBUF_BUFFER[16] = LITRES[3] + 0x30;
   SBUF_BUFFER[17] = LITRES[2] + 0x30;
   SBUF_BUFFER[18] = LITRES[1] + 0x30;
   SBUF_BUFFER[19] = LITRES[0] + 0x30;


   SBUF_BUFFER[20]=0x30;
   SBUF_BUFFER[21]=0x31;                // "1" ... 100
   SBUF_BUFFER[22]=0x30;                // "0"
   SBUF_BUFFER[23]=0x30;                // "0"

   SBUF_BUFFER[24] = 0x03;              // ETX
 
   LRC = (SBUF_BUFFER[2] ^ SBUF_BUFFER[3]);
   for(loop=4;loop<=24;loop++)
   {
       LRC = (LRC ^ SBUF_BUFFER[loop]);
       watchdog();
   }

   SBUF_BUFFER[25] = LRC;

   COMMS_TX=1;                        // enable transmit 
   timer(10);

   ES = 0;
   RI = 0;
   TI = 0;

   for(loop=0;loop<=25;loop++)
   {
      TI=0;
      SBUF=SBUF_BUFFER[loop];
      while(!TI); 
      watchdog();                   // watchdog 
   }

   COMMS_TX=0;                      // disable transmit 
}



//................... E04 ............................................. 
void halt(void)                     // opcode E04 
{
   ES=0;
   stat=5;                          // pump halted 
   EX1=0;                           // disable intterupt 
   MOTOR = 1;                   	// relay off 

   SBUF_BUFFER[2]=0x65;             // "e" 

 // SBUF_BUFFER[2]=.....            // response ........ see above 
   SBUF_BUFFER[3]=nozzle_id2;    	// pump number - LSD ONLY !!!
   if(SBUF_BUFFER[4]==0x5a) SBUF_BUFFER[3]=0;			// "NULL" if set_ppl 

   SBUF_BUFFER[4]=0x03;             // ETX             	// trailer 
   LRC_calc();              		// LRC             	// trailer 

   COMMS_TX=1;                      // transmit enable 
   timer(100);

// the folowing commands are for the serial transmission of the data 

   for(loop=0;loop<=4;loop++)
   {
      TI=0;
      SBUF=SBUF_BUFFER[loop];
      while(!TI);
      watchdog();                   // watchdog 
   }

   TI=0;
   SBUF=LRC;
   while(!TI);

   watchdog();                  	// watchdog 

   COMMS_TX=0;                    	// disable transmit 

   // added by HCB 2000 03 22 for completely resetting the DCM after the transaction
   DCM_APPROVED = 0;
}



/* // .......................... E05 .................................... 
void set_ppl(void)
{

   if(NOZZLE_SW==1)
   {
      stat=1;
      SBUF_BUFFER[2]=0x41;        // idle 
   }

   else if(NOZZLE_SW==0)
   {
      stat=2;
      SBUF_BUFFER[2]=0x42;        // waiting for approval 
   }


   SBUF_BUFFER[4]=0x5a;      // "Z" ... for comparison test 
   reset_pulse();         // reset count 
   timer(300);
   fuel1=0;

   SBUF_BUFFER[3]=address + 0x30;               //                 // pump number 
   if(SBUF_BUFFER[4]==0x5a) SBUF_BUFFER[3]=0;         // "NULL" if set_ppl 

   SBUF_BUFFER[4]=0x03;                  // ETX             // trailer 
   LRC_calc();              // LRC             // trailer 

   COMMS_TX=1;                            // transmit enable 
   timer(100);

// the folowing commands are for the serial transmission of the data 


   for(loop=0;loop<=4;loop++)
   {
      TI=0;
      SBUF=SBUF_BUFFER[loop];
      while(!TI);
      watchdog();                          // watchdog 

   }

   TI=0;
   SBUF=LRC;
   while(!TI);

   watchdog();                  // watchdog 

   COMMS_TX=0;                    // disable transmit 
}  */



// ...................... SWITCH MOTOR ONLY OFF ... NEW opcode E10 ............................
void pump_pause(void)
{
   MOTOR = 1;                                    // MOTOR OFF
   COMMS_TX = 1;                                 // enable RS485 TX
   for(loop=0;loop<=8;loop++)                    // transmit 9 characters
   {
     TI = 0;
     SBUF = SBUF_BUFFER[loop];
     while(!TI);
     ////watchdog();
   }
   COMMS_TX = 0;                                 // disable RS 485 TX
}



// ..... CONTINUE  ONLY MOTOR BACK ON .... NEW opcode E11 ...................................
void pump_continue(void)
{
   MOTOR = 0;                                    // REALY ON
   COMMS_TX = 1;                                 // enable RS485 TX   

   for(loop=0;loop<=8;loop++)                    // transmit 9 characters
   {
     TI = 0;
     SBUF = SBUF_BUFFER[loop];
     while(!TI);
     ////watchdog();
   }
   COMMS_TX = 0;                                 // disable RS 485 TX 
}



// ................ NEW opcode E20 ........
void pump_type(void)                             // NEW opcode E20
{
  write_rom(1,(SBUF_BUFFER[7] - 0x30));                  // store new value in EEPROM

  timer(1000);
  write_rom(3,(SBUF_BUFFER[8] - 0x30));
  timer(1000);
  write_rom(4,(SBUF_BUFFER[9] - 0x30));
  timer(1000);
  write_rom(5,(SBUF_BUFFER[10] - 0x30));
  timer(1000);
  write_rom(6,(SBUF_BUFFER[11] - 0x30));
  timer(1000);
  write_rom(7,(SBUF_BUFFER[12] - 0x30));
  timer(1000);

  EA = 0;

  COMMS_TX = 1;                                 // enable TX
  for(loop=0;loop<=13;loop++)                   // transmit 14 characters
  {
     TI = 0;
     SBUF = SBUF_BUFFER[loop];
     while(!TI);
  }
  COMMS_TX = 0;                                 // disable TX

  pulser_ratio = ((read_rom(3) * 10000) + (read_rom(4) * 1000) + (read_rom(5) * 100) + (read_rom(6) * 10) + read_rom(7));
}



// ...................... READ DEVICE SERIAL No... NEW opcode E23 ............................
void read_serial_no(void)
{
   SBUF_BUFFER[7] = read_rom(11);                // read device serial No - MSB
   SBUF_BUFFER[8] = read_rom(12);                // read device serial No
   SBUF_BUFFER[9] = read_rom(13);                // read device serial No
   SBUF_BUFFER[10] = read_rom(14);               // read device serial No
   SBUF_BUFFER[11] = read_rom(15);               // read device serial No
   SBUF_BUFFER[12] = read_rom(17);               // read device serial No
   SBUF_BUFFER[13] = read_rom(18);               // read device serial No
   SBUF_BUFFER[14] = read_rom(19);               // read device serial No
   SBUF_BUFFER[15] = read_rom(20);               // read device serial No - LSB

   SBUF_BUFFER[16] = 0x03;                       // ETX

   LRC = (SBUF_BUFFER[2] ^ SBUF_BUFFER[3]);
   for(loop=4;loop<=15;loop++)
   {
      LRC = (LRC ^ SBUF_BUFFER[loop]);
      watchdog();
   }
   
   SBUF_BUFFER[16] = LRC;                        // LRC
 
   COMMS_TX = 1;                                 // enable RS485 TX
   for(loop=0;loop<=16;loop++)                    // transmit 9 characters
   {
     TI = 0;
     SBUF = SBUF_BUFFER[loop];
     while(!TI);
     ////watchdog();
   }
   COMMS_TX = 0;                                 // disable RS 485 TX
}



// ...................... set DEVICE SERIAL No... NEW opcode E24 ............................
void set_serial_no(void)
{
   if(read_rom(2) != 0x5a)               // check if serial number has already been propgrammed
   {
      write_rom(11,SBUF_BUFFER[7]);       // write device serial No - MSB
      write_rom(12,SBUF_BUFFER[8]);       // write device serial No
      write_rom(13,SBUF_BUFFER[9]);       // write device serial No
      write_rom(14,SBUF_BUFFER[10]);      // write device serial No
      write_rom(15,SBUF_BUFFER[11]);      // write device serial No
      write_rom(17,SBUF_BUFFER[12]);      // write device serial No
      write_rom(18,SBUF_BUFFER[13]);      // write device serial No
      write_rom(19,SBUF_BUFFER[14]);      // write device serial No
      write_rom(20,SBUF_BUFFER[15]);      // write device serial No - LSB
      
      write_rom(2,0x5a);                  // indicator that serial number is programmed
   }
   
 // !! DOES NOT ECHO OUT SERIAL No ... BUT WHAT IS ACTUALLY PROGRAMMED IN EEPROM

   SBUF_BUFFER[7] = read_rom(11);                // read device serial No - MSB
   SBUF_BUFFER[8] = read_rom(12);                // read device serial No
   SBUF_BUFFER[9] = read_rom(13);                // read device serial No
   SBUF_BUFFER[10] = read_rom(14);               // read device serial No
   SBUF_BUFFER[11] = read_rom(15);               // read device serial No
   SBUF_BUFFER[12] = read_rom(17);               // read device serial No
   SBUF_BUFFER[13] = read_rom(18);               // read device serial No
   SBUF_BUFFER[14] = read_rom(19);               // read device serial No
   SBUF_BUFFER[15] = read_rom(20);               // read device serial No - LSB

   SBUF_BUFFER[16] = 0x03;                       // ETX

   LRC = (SBUF_BUFFER[2] ^ SBUF_BUFFER[3]);
   for(loop=4;loop<=15;loop++)
   {
      LRC = (LRC ^ SBUF_BUFFER[loop]);
      watchdog();
   }
   
   SBUF_BUFFER[16] = LRC;                        // LRC
 
   COMMS_TX = 1;                                 // enable RS485 TX
   for(loop=0;loop<=16;loop++)                    // transmit 9 characters
   {
     TI = 0;
     SBUF = SBUF_BUFFER[loop];
     while(!TI);
     ////watchdog();
   }
   COMMS_TX = 0;                                 // disable RS 485 TX
}



// ...................... READ DEVICE ID ... NEW opcode E97 ............................
void read_id(void)
{
   LED = 1;

   SBUF_BUFFER[6] = read_rom(10);                // read device ID

   COMMS_TX = 1;                                 // enable RS485 TX
   for(loop=0;loop<=8;loop++)                    // transmit 9 characters
   {
     TI = 0;
     SBUF = SBUF_BUFFER[loop];
     while(!TI);
     ////watchdog();
   }
   COMMS_TX = 0;                                 // disable RS 485 TX

  timer(1000);
  LED = 0;
}


// ...................... WRITE DEVICE ID ... NEW opcode E98 ............................
void set_id(void)
{
	LED = 1;

   	write_rom(10,SBUF_BUFFER[6]);                 // store new device ID in HEX
   
   	COMMS_TX = 1;                                 // enable RS485 TX
   	for(loop=0;loop<=8;loop++)                    // transmit 9 characters
   	{
    	TI = 0;
    	SBUF = SBUF_BUFFER[loop];
    	while(!TI);
    	//watchdog();
	}
   	COMMS_TX = 0;                                 // disable RS 485 TX

  	timer(1000);
  	LED = 0;           
  	nozzle_id = read_rom(10);                      // read new DEVICE ID 
  
  	nozzle_id1 = nozzle_id / 10;     // MSD
  	nozzle_id2 = nozzle_id % 10;     // LSD 
     
  	watchdog();
     
  	nozzle_id1 += 0x30;
  	nozzle_id2 += 0x30;
}



// ........................ E99 --- SOFTWARE VERSION ...................................
void software(void)
{
   ES=0;                                   // disable comms interrupt

   //watchdog();                           // watchdog

   SBUF_BUFFER[0] = 0x02;                  // STX             // header
   SBUF_BUFFER[1] = 0x32;                  // SEQ CHAR        // header
   SBUF_BUFFER[2] = 0x50;                  // "P"
   SBUF_BUFFER[3] = 0x65;                  // "e"
   SBUF_BUFFER[4] = 0x74;                  // "t"
   SBUF_BUFFER[5] = 0x72;                  // "r"
   SBUF_BUFFER[6] = 0x6f;                  // "o"
   SBUF_BUFFER[7] = 0x4d;                  // "M"
   SBUF_BUFFER[8] = 0x61;                  // "a"
   SBUF_BUFFER[9] = 0x6e;                  // "n"
   SBUF_BUFFER[10] = 0x44;                 // "D"
   SBUF_BUFFER[11] = 0x43;                 // "C"
   SBUF_BUFFER[12] = 0x4d;                 // "M"
   SBUF_BUFFER[13] = 0x35;                 // "5"
   SBUF_BUFFER[14] = 0x2E;                 // "."
   SBUF_BUFFER[15] = 0x32;                 // "2"
   SBUF_BUFFER[16] = 0x30;                 // "0"

   SBUF_BUFFER[17] = 0x03;                 // ETX             // trailer

   LRC = (SBUF_BUFFER[2] ^ SBUF_BUFFER[3]);
   for(loop=4;loop<=17;loop++)
   {
      LRC = (LRC ^ SBUF_BUFFER[loop]);
      watchdog();
   }
   
   SBUF_BUFFER[18] = LRC;

   COMMS_TX = 1;                         // transmit enable
   timer(100);

	// the folowing commands are for the serial transmission of the data
   for(loop = 0;loop <= 18;loop++)
   {
       TI = 0;
       SBUF = SBUF_BUFFER[loop];
       while(!TI);
       //watchdog();
   }

   COMMS_TX = 0;                    // disable transmit
}



// ..................... opcode E40 ....... emulate NIU answer ..............................
void send_NIU_data(void)
{
   ES=0;                    // disable comms interrupt

   if((vcu_read == 1) || (tag_read == 2))
   {
     //watchdog();                 // watchdog

     SBUF_BUFFER[0] = 0x02;                  // STX             // header
     SBUF_BUFFER[1] = 0x31;                  // "1"
     SBUF_BUFFER[2] = 0x45;                  // "E"                 
     SBUF_BUFFER[3] = 0x34;                  // "4"
     SBUF_BUFFER[4] = 0x30;                  // "0"
     SBUF_BUFFER[5] = nozzle_id1;            // pump No - MSD
     SBUF_BUFFER[6] = nozzle_id2;            // pump No - LSD  

     watchdog();
     
	// key control VCU format detected so transmit 65 characters to DCM

     if((TAG_BUFFER[57]==0x15)&&(TAG_BUFFER[58]==0x0d)&&(TAG_BUFFER[59]==0x0a))
     {
		for(loop=0;loop<=52;loop++)
     	{
            watchdog();
        	SBUF_BUFFER[loop + 7] = TAG_BUFFER[loop + 1];
     	}
     
     	watchdog();
     
     	SBUF_BUFFER[60] = 0x03;               // ETX
     	
		//        crc16(0,60);							// crc is from STX to ETX

      
		//     	SBUF_BUFFER[61] = (CRC16 & 0x00ff);		// low byte of crc
		//     	SBUF_BUFFER[62] = (CRC16 >> 8);     // hi byte of crc
     	
		//        SBUF_BUFFER[63] = 0x15;		// indicate previous bytes are CRC

		LRC=0;
 		for(loop=1;loop<=59;loop++)
  		{
   			LRC=LRC^SBUF_BUFFER[loop];
   	 	}

		SBUF_BUFFER[61] = LRC;
   		
		//        SBUF_BUFFER[64] = 0x0d;     // <CR>
		//        SBUF_BUFFER[65] = 0x0a;     // <LF>
   
     	timer(2000);
     	COMMS_TX = 1;                         // transmit enable
     	timer(100);

     	for(loop = 0;loop <= 61;loop++)
     	{
        	TI = 0;
        	SBUF = SBUF_BUFFER[loop];
        	while(!TI);
       		watchdog();
     	}
     }
     
	// syntrol format tag or vcu detected so transmit 37 characters for NIU number

     if((TAG_BUFFER[38]==0x20)&&(TAG_BUFFER[39]==0x0d)&&(TAG_BUFFER[40]==0x0a))
     {
		for(loop=0;loop<=36;loop++)
     	{
        //watchdog();
        	SBUF_BUFFER[loop + 7] = TAG_BUFFER[loop + 1];
     	}
     
     	LRC = (SBUF_BUFFER[1] ^ SBUF_BUFFER[2]);
     
     	watchdog();
     
     	for(loop=3;loop<=43;loop++)
     	{
        //watchdog();
        	LRC = (LRC ^ SBUF_BUFFER[loop]);
     	}
     	SBUF_BUFFER[44] = 0x03;               // ETX
     	SBUF_BUFFER[45] = LRC;
   
     	timer(2000);
     	COMMS_TX = 1;                         // transmit enable
     	timer(100);

     	for(loop = 0;loop <= 45;loop++)
     	{
        	TI = 0;
        	SBUF = SBUF_BUFFER[loop];
        	while(!TI);
     	}
     }

     COMMS_TX = 0;                    // disable transmit
     clear_SBUF();
   } 
}



// .............................. clear SBUF array ........................................
void clear_SBUF(void)
{
  for(loop=0;loop<=82;loop++)
  {
     watchdog();
     SBUF_BUFFER[loop] = 0;   
  }
}



// ..................................................................... 
void fuel_calc(void)
{
 unsigned long data temp_fuel;
 
 watchdog();
 
 LITRE_BUSY = 1;                  // HCB
             
 //Fuel_Still_Busy = 0;			  // 2016/06/22 Philip Foster added fuel still flowing
			                        
 fuel1 = COUNTER.pulse;  

   if(fuel1 > 0)
   {
      temp_fuel = (fuel1 * 1000L);    // JP changed from 10000L 00/09/22
      fuel1 = (temp_fuel / (unsigned long)pulser_ratio);
   }

   //if(fuel1 > 900000)				 // Added to stop SCM reaching MAXVOL 	2016/11/02
 	//MOTOR = 1;

  // new_tank_cap_percentage = new_tank_cap_percentage * 10000;
   if(fuel1 > new_tank_cap_percentage)//9000.00
   MOTOR = 1;

   /* HO Start at most significant digit */
   LITRES[7] = (fuel1  / 10000000L);

                                     // 10
   temp_fuel = ((unsigned long)LITRES[7] * 10L);
   LITRES[6] = (fuel1 / 1000000L)  - temp_fuel;
                                     // 100
   temp_fuel = (temp_fuel + (unsigned long)LITRES[6]) * 10L;
   LITRES[5] = (fuel1 / 100000L)   - temp_fuel;
                                     // 1000
   temp_fuel = (temp_fuel + (unsigned long)LITRES[5]) * 10L;

   LITRES[4] = (fuel1 / 10000L)    - temp_fuel;
                                     // 10000
   temp_fuel = (temp_fuel + (unsigned long)LITRES[4]) * 10L;

   LITRES[3] = (fuel1 / 1000L)     - temp_fuel;

                                     // 100000
   temp_fuel = (temp_fuel + (unsigned long)LITRES[3]) * 10L;
   LITRES[2] = (fuel1 / 100L)      - temp_fuel;
                                     // 1000000
   temp_fuel = (temp_fuel + (unsigned long)LITRES[2]) * 10L;

   LITRES[1] = (fuel1 / 10L)       - temp_fuel;

   temp_fuel = (temp_fuel + (unsigned long) LITRES[1]) * 10L;

   LITRES[0] = fuel1              - temp_fuel; 
 
   watchdog();

   LITRE_BUSY=0;		//HCB

     // ............VOLUME  limit if display 999,99 ...... 
 /*  if((fuel1 >= 98000) && (fuel1 < 99900))      // 990,00 liters 
   {
       MOTOR = 1;           // relay off 

       EX1=0;                     // disable pulser interrupt 
       stat=1;                    // pump idle 
       a=1;                       // nozzle lift variable 
       b=0;                       // reset time variable 
       c=0;                       // reset time variable 
   }*/

   watchdog();

   if((OVER_RIDE==1)&&(NOZZLE_SW==0))  // if over-ride is off - normal operation 
   {                         // and nozzle lifted 
	nozzle_down=0;          // reset debounce variable 
	watchdog();                              // reset watchdog timer 
   }
}



// ..................................................................... 
void LRC_calc(void)             // LRC calculations 
{
   watchdog();                  // watchdog 

// each character to be sent is EX ORed with the next character 
// process after STX and SEQ. CHAR 

   LRC=(SBUF_BUFFER[2] ^ SBUF_BUFFER[3]);
   LRC=(LRC ^ SBUF_BUFFER[4]);

   watchdog();                 // watchdog 
}



// ................................................................. 
void over_ride(void)
{ 
	LED = 1;

   	while(OVER_RIDE==0)
   	{
		ES=0;
	
		while((NOZZLE_SW==1)&&(OVER_RIDE==0)) // wait here until nozzle ...
		{                      // .. is lifted or over ride .... 
	    	watchdog();         // .. is de-activated .... 
	    	timer(500);         // .. also keep reseting watchdog 
		}
    	
		watchdog();
		/*
		if(NOZZLE_SW==0)
		{
	  		reset_pulse();          // reset pulser count 
      		timer(2000);

	  		fuel_calc();
		} */

		while(NOZZLE_SW==0)         // while nozzle is lifted 
		{
	     	watchdog();            // watchdog signal 
         	if(OVER_RIDE == 1) 
         	{
         		MOTOR = 1;          // switch MOTOR OFF 
            	break;
         	}
         	if(NOZZLE_SW==0)
	     	{
	     		//	 fuel_calc();
		 		timer(30000);         // additional delay before motor switch on 
	     		//	 EX1=0;                // disable interrupt 

		 		MOTOR = 0;
	     		//	 timer(10000);
	     		//	 IE1 = 0;                // clear interrupt flag 
	     		//	 EX1 = 1;                // enable interrupt 
            	//    ES = 0;                 // disable comms interrupt
            	//    EA = 1;                 // enable global interrupts
			}
	    	// over ride + nozzle up  - pump on 

	   		// fuel_calc();        // read pulser 

	    	watchdog();           // watchdog signal 

	    	nozzle_down=0;

	    	while((nozzle_down <= 20) && (OVER_RIDE==0)) // wait here until nozzle ...
	    	{                      // .. is returned or over ride .... 
				watchdog();

		 		//if((NOZZLE_SW==1)&&(Fuel_Still_Flowing==0)) nozzle_down++;
		 		if (NOZZLE_SW==1) nozzle_down++;
		 		else if (NOZZLE_SW==0) nozzle_down=0;

            	//    fuel_calc();
	      	}

	    	if(NOZZLE_SW==1)   // nozzle returned 
	    	{
	    		//	 fuel_calc();      // read fuel used 
		 		EX1=0;            // disable intterupt 
		 		MOTOR = 1;  // pump off 
		 		watchdog();

		 		timer(50000);    // do not allow quick restart 
		 		timer(50000);
		 		while(NOZZLE_SW==0) watchdog();   // wait for nozzle release 
   	    	}
		}
	}

  	while(NOZZLE_SW == 0) watchdog();     // if nozzle lifted + overide switched off ....
  
  	COMMS_TX=0;           // enable comms 
  	ES = 1;
  
  	LED = 0;
}



// ........................... clear ALL variables ............................................
void clear_ALL(void)
{
   stat=1;                       // status variable - pump idle 
   EX1=0;                        // disable intterupt 
   MOTOR = 1;                    // relay off 
   nozzle_down = 80;             // keep from rolling over
   a=0;                          // nozzle lift variable 
   b=0;                          // reset time variable 
   c=0;                          // reset time variable 
   OSC_CONTROL = 1;              // switch TAG osc. OFF
   RED_LED = 0;                  // RED LED OFF
                   
   SYSTEM_COMMS = 0;             // switch 4066 channel OFF
   NOZZLE_COMMS = 0;             // switch 4066 channel OFF
   TAG_COMMS    = 0;             // switch 4066 channel OFF
 
   TR0 = 0;                      // flashing LED timer OFF
   TF0 = 0;                      // clear interrupt flag
   ET0 = 0;                      // TIMER0 interrupt OFF
   LED = 0;                   	 // buzzer OFF  
              
   tag_read = 0;                 // reset tag read variable
   vcu_read = 0;
   DCM_APPROVED = 0;

   for(loop=0;loop<=41;loop++)
   {
     TAG_BUFFER[loop] = 0;            // clear buffer
     watchdog();
   }

   for(loop=0;loop<=25;loop++)
   {
     SBUF_BUFFER[loop] = 0;            // clear buffer
     watchdog();
   }

   timeout = nozzle_timeout = 0;       // reset timeouts
   vcu_tag = 0;                        // indicator if tag + vehicle
   first_read = 0;
   clear_SBUF();
}



// ................................. CLEAR POWER FAIL DATA ....................................
void clear_power_fail(void)
{  
     write_rom(25,0);   // stored MSD
     write_rom(26,0);              
     write_rom(27,0);                 
     write_rom(28,0);   // stored LSD

     COUNTER.FLOW[0] = 0;
     COUNTER.FLOW[1] = 0;
     COUNTER.FLOW[2] = 0;
     COUNTER.FLOW[3] = 0;
     
     reset_pulse();
     
     if (LITRE_BUSY == 0) fuel_calc();	//HCB
}



// ................................. START OF MAIN PROGRAMM ..................................... 
main(void)
{
   NZ_2WIRE_TX = 0;      // keep nozzle RS485 in RX ....
   EA = 0;
   sys_init();           // initialise system      
   comms_init();         // initialise comms         // initialise last    	

   watchdog();

   LITRE_BUSY=0;

   int_init();           // initialise interrupts 

   LED = 1;

   reset_pulse();        // clear all pulser variables


   COUNTER.FLOW[0] = read_rom(25);        // read from EEPROM - MSD ....
   COUNTER.FLOW[1] = read_rom(26);        // read from EEPROM
   COUNTER.FLOW[2] = read_rom(27);        // read from EEPROM
   COUNTER.FLOW[3] = read_rom(28);        // read from EEPROM - LSD ....


   fuel_calc();          // calculate litre values

   watchdog();
 
   clear_ALL();          // clear all arrays ....

   stat=1;
   tag_read = 0;
   vcu_read = 0;
   DCM_APPROVED = 0;
   VCU_PRESENT_Last = VCU_PRESENT;

//*****************set ID******************************************************************************************//
ID_8=0;
ID_7=0;
ID_6=0;
ID_5=0;
ID_4=0;
ID_3=0;
ID_2=0;

if (ID_8==1)
{SBUF_BUFFER[6]=8;
ID_LOOP=8;
}
if (ID_7==1)
{SBUF_BUFFER[6]=7;
ID_LOOP=7;
}
if (ID_6==1)
{SBUF_BUFFER[6]=6;
ID_LOOP=6;
}
if (ID_5==1)
{SBUF_BUFFER[6]=5;
ID_LOOP=5;
}
if (ID_4==1)
{SBUF_BUFFER[6]=4;
ID_LOOP=4;
}
if (ID_3==1)
{SBUF_BUFFER[6]=3;
ID_LOOP=3;
}
if (ID_2==1)
{SBUF_BUFFER[6]=2;
ID_LOOP=2;
}
if (!(ID_2==1)&&!(ID_3==1)&&!(ID_4==1)&&!(ID_5==1)&&!(ID_6==1)&&!(ID_7==1)&&!(ID_8==1))
{SBUF_BUFFER[6]=1;
ID_LOOP=1;
}

set_id();
read_id();

COMMS_TX=0;
LED=SBUF_BUFFER[6];
        LED = 1;
        RED_LED = 1;
      timer(5000);
      timer(5000);
      timer(5000);
      timer(5000);

     for(loop2=0;loop2<ID_LOOP;loop2++)
     {timer(5000);
      timer(5000);
     for(loop=0;loop<=15;loop++)
     {
        LED = 1;
        RED_LED = 1;
        timer(5000);
        LED = 0;
        RED_LED = 0;
        timer(5000);

     }}
P0=SBUF_BUFFER[6];

// ...................... START ......................................... 

		MOTOR = 1;

	    while(1)
	    {
			watchdog();
			fuel_calc();

			COMMS_TX=0;        // disable TX of 75176 
			
			if(OVER_RIDE==0)   over_ride();  // over ride activated 

			watchdog();         // watchdog 

			TI  = 0;            // reset transmit interupt 
			RI  = 0;            // reset receive interupt  
			IE0 = 0;            // clear interrupt flag
			REN = 1;            // set enable receive      
			EA  = 1;            // enable all interrupts  
			IE0 = 0;            // clear interrupt flag		
			ES  = 1;            // enable comms interrupt
        	EX0 = 1;            // enable INT0
        	LED = 0;

			//.........................................................................
			//   if(stat==1)  "A" .... pump idle
			//   if(stat==2)  "B" .... requesting approval
			//   if(stat==3)  "C" .... sale in progress - slow flow
			//   if(stat==4)  "D" .... no price per litre
			//   if(stat==5)  "E" .... pump halted
			//   if(stat==6)  "F" .... approved - no valves open
			//   if(stat==7)  "G" .... sale in progress - fast flow 
			//.........................................................................

			if(stat==5) stat=1;   // if pump  halted - make idle 

			if((NOZZLE_SW==0)&&(a==0)&&(stat==1)) // nozzle lift sensor 
			{
		    	stat=2;                      // status variable - request approval 
		    	a=1;                         // nozzle lift variable 
                OSC_CONTROL = 0;             // switch TAG osc. ON
                RED_LED = 1;                 // RED LED ON
                timer(5000);
                    
                TL0 = 0;
                TH0 = 0;
                TR0 = 1;                     // switch TIMER0 ON
                ET0 = 1;                     // enable TIMER0 interrupt  
			}

			if ((NOZZLE_SW==1)&&(Fuel_Still_Flowing==0)) 
			{
				nozzle_down++;     // nozzle returned ....   & Fuel not flowing - Philip Foster 2016/06/22
			}
            else  
			{
				if(NOZZLE_SW==0) 
				{
					nozzle_down=0;
				}
			}

			if (NOZZLE_SW==1) 
			{
				MOTOR = 1;									// If nozzle down then stop motor - Philip Foster 2016/06/22
			}
				
			if (Fuel_Still_Flowing > 0) 
			{
				Fuel_Still_Flowing--;	    		// 2016/06/22 Philip Foster added clear fuel still flowing flag
			}

			if(nozzle_down >= 5000)		 // This used to be 80 HCB 05042000	   2016/08/25 Philip Foster changed to 5000 to cater for slowing pulses after pump has stopped
			{                                // ... after finnished  
		    	clear_ALL();
                nozzle_down = 0;
                while(NOZZLE_SW == 1)
                { 
                	watchdog();  						// while nozzle down
                    if(OVER_RIDE==0)   over_ride();  	// over ride activated 
                    REN = 1;
                    RI = TI = 0;
                    ES = 1;
                    EA = 1;
                    EX0 = 1;  
                }
                timer(65000);            // delay after nozzle lifted ... 
			}

			watchdog();                      // watchdog 

			// the following code determines "FAST" or "SLOW" flow ..... 
			if((stat > 2)&&(stat!=5))        // if pump is in fast or slow flow.... 
			{                                // .... but not halted 
		   		if(fuel1==fuel2) 
				{
					stat=3;      // slow flow 
				}
		   		if(fuel1 != fuel2) 
				{
					stat=7;    // fast flow 
				}
			}
			if(c==5) 
			{
				fuel2=fuel1;       // .... flow rate 
			}
			b++;          				// increment time variable 
			if(b==30)
			{
		   		c++;                     // update time variable 
		   		b=0;                     // reset time variable 
			}
			if(c==30)
			{
		   		c=0;                     // reset time variable 
		  		b=0;                     // reset time variable 
			}

        	// port pin ... data present
        	if((VCU_PRESENT==0)&&(TAG_PRESENT==1)&&(TAG_COMMS==0)&&(tag_read==0)&&(vcu_read==0)&&(NOZZLE_SW==0)) 
        	{
        		timer(2);
            	if(VCU_PRESENT == 1)
            	{
            		BAUDRATE = 24;    // indicator only
                	baud24;
                	NOZZLE_COMMS = 1; // 4066 channel ON 
                	SYSTEM_COMMS = 0;
                	TAG_COMMS = 0;
                	EX0 = 0;          // disable system comms 
                	ES = 1;
                	EA = 1;
				}
			}
                
			// port pin ... data present                                                        
        	if((TAG_PRESENT==0)&&(VCU_PRESENT==1)&&(NOZZLE_COMMS==0)&&(vcu_read==0)&&(DCM_APPROVED==0)&&(NOZZLE_SW==0))
        	{
        		BAUDRATE = 24;    // indicator only
            	baud24;
            	TAG_COMMS = 1;    // 4066 channel ON
            	SYSTEM_COMMS = 0;
            	NOZZLE_COMMS = 0;
            	EX0 = 0;          // disable system comms 
            	ES = 1;
            	EA = 1;
			}

			if((VCU_PRESENT == 0) && (tag_read != 0)) vcu_tag++;
                
        	watchdog();
                
        	if(vcu_tag >= 4) 
        	{
        		MOTOR = 1;                        // motor off - TAGGED operation detected VCU
                    
            	while(NOZZLE_SW == 0)  // wait for nozzle return    
            	{
            		watchdog();
            	}
        	}

        	if((VCU_PRESENT == 1) && (vcu_tag > 0)) 	
			{	
				vcu_tag--; // reduce count if no data
			}
			
			if(VCU_PRESENT != VCU_PRESENT_Last)       // port pin ... data present
        	{
            	timeout = 0;
               	nozzle_timeout = 0;
				VCU_PRESENT_Last = VCU_PRESENT;
        	} 

			if((DCM_APPROVED == 1) && (vcu_read == 1)) 
			{
				timeout++;
			}

       		if(timeout >= 1)   // 10 // 60
        	{
        		nozzle_timeout++;
            	timeout = 0;
        	}

			if((nozzle_timeout >= nozzle_delay) && (MOTOR == 0))
        	{
        		//  EX1 = 0;                  // disable interrupt 
            	MOTOR = 1;                // MOTOR OPEN ... nozzle removed
            	vcu_read = 0;             // allow nozzle comms to work
            	nozzle_timeout = nozzle_delay;
        	}
                
        	//  sound buzzer if nozzle removed ...
        	// if((DCM_APPROVED == 1) && (MOTOR == 1)) LED = 1;
        } 
}

// ........................... END ...................................... 

